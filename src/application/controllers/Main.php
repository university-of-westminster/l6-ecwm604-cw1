<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends EXT_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();

        $this->load->model("posts");
    }

	public function index($page = 1)
    {
        $total = $this->posts->getTotal($this->session->sort);

        if (($page < 1) || ($page > $this->posts->hlp_pagesFromTotal($total))){
            redirect("");
        }

        $this->load->view("header");
        $this->load->view(
            "index_view",
            array(
                "posts"     => $this->posts->getAll($page, $this->session->sort, $this->session->username),
                "total"     => $total,
                "userPosts" => $this->posts->getUserPostsCount($this->session->username),
                "userVotes" => $this->posts->getUserVotesCount($this->session->username),
                "page" => $page
            )
        );
        $this->load->view("footer");
    }

    public function post($post_id){
        $this->load->library("gui");
        $post = $this->posts->getPost($post_id, $this->session->username, true);

        if ($post == NULL){
            redirect("");
        }

        $this->load->view("header");
        $this->load->view(
            "post_view",
            array(
                "post"      => $post,
                "userPosts" => $this->posts->getUserPostsCount($this->session->username),
                "userVotes" => $this->posts->getUserVotesCount($this->session->username),
                "comments"  => $this->posts->getTopLevelComments($post_id)
            )
        );
        $this->load->view("footer");
    }

    public function vote($post_id, $vote){
        if ($vote <= 2 || $vote >= 0){
            if ($vote == 2){ $vote = -1; }
            $this->posts->vote($post_id, $this->session->username, $vote);
        }
    }

    public function submit(){
        $this->load->view("header");
        $this->load->view(
            "submit_view",
            array(
                "userPosts" => $this->posts->getUserPostsCount($this->session->username),
                "userVotes" => $this->posts->getUserVotesCount($this->session->username)
            )
        );
        $this->load->view("footer");
    }

    public function submitpost(){
        if (
            ($this->input->post("content") == NULL)     ||
            (trim($this->input->post("content")) == "") ||
            ($this->input->post("title") == NULL)       ||
            (trim($this->input->post("title")) == "")
        ){
            $this->session->errors = array("You either forgot to fill in some fields, or your input data was invalid");
            $this->session->mark_as_flash("errors");
        }
        else{
            $this->posts->createPost($this->input->post("content"), $this->input->post("title"), $this->session->username);
            $this->session->notifications = array("Post successfuly created");
            $this->session->mark_as_flash("notifications");
        }

        redirect("");
    }

    public function submitcomment(){
        if  (
          ($this->input->post("comment") == NULL) ||
          (trim($this->input->post("comment")) == "") ||
          ($this->input->post("post_id") == NULL) ||
          (!is_numeric($this->input->post("post_id")))
        ){
            $comment_id = "unknown";
        } else{
            $this->posts->putComment($this->input->post("post_id"), $this->session->username, $this->input->post("parent"), $this->input->post("comment"));
        }
            redirect(sprintf("/post/%d#comments", $this->input->post("post_id")));
    }

    public function votes($post_id){
        echo json_encode($this->posts->getVotes($post_id));
    }

    public function login(){
        if ($this->input->post("username") != NULL){
            $this->session->username = strtolower($this->input->post("username"));
        }

        redirect("");
    }

    public function logout(){
        unset($_SESSION["username"]);
        redirect("");
    }

    public function generate(){ // Generates a random post
        $this->posts->_generatePost();
        redirect("");
    }

    public function simulate(){ // Simulates voting
        $this->posts->_simulateVoting(0.6);
    }

    public function sort($sort_id){
        if (!array_key_exists($sort_id, $this->posts->sortModes)){
            $this->session->sort = SORT_DEFAULT;
        } else{
            $this->session->sort = $sort_id;
        }

        redirect("");
    }
}
