<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EXT_Controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct(){
        parent::__construct();
        $this->_checkSort();
        $this->_checkLogin();
    }

    private function _checkLogin(){
        if (($this->router->method != "index") && ($this->router->method != "login")){
            if ($this->session->username == NULL){
                redirect("");
            }
        }
    }

    private function _checkSort(){
        if ($this->session->sort == NULL){
            $this->session->sort = SORT_DEFAULT;
        }
    }
}
