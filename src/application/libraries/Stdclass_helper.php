<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stdclass_helper{
    public function createStdClass(array $a){
        $output = new stdClass();
        foreach ($a as $key => $value){
                $output->$key = (is_array($value))? ($this->createStdClass($value)) : ($value);
        }
    }
}
