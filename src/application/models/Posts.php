<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Model{
    private $_usernames = ["james","john","robert","michael","mary","william","david","richard","charles","joseph","thomas","patricia","christopher","linda","barbara","daniel","paul","mark","elizabeth","donald","jennifer","george","maria","kenneth","susan","steven","edward","margaret","brian","ronald","dorothy","anthony","lisa","kevin","nancy","karen","betty","helen","jason","matthew","gary","timothy","sandra","jose","larry","jeffrey","frank","donna","carol","ruth","scott","eric","stephen","andrew","sharon","michelle","laura","sarah","kimberly","deborah","jessica","raymond","shirley","cynthia","angela","melissa","brenda"];
    public $sortModes = [];

    public function __construct(){
        parent::__construct();

        // $this->load->library("stdclass_helper");

        $this->sortModes = [
            (object)[
                "title"     => "Popularity (all time)",
                "getFunc"   => "getPosts_popularityAll",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Popularity (last day)",
                "getFunc"   => "getPosts_popularityDay",
                "countFunc" => "countPosts_popularityDay"
            ],
            (object)[
                "title"     => "Date",
                "getFunc"   => "getPosts_date",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "Title",
                "getFunc"   => "getPosts_title",
                "countFunc" => "countPosts_all"
            ],
            (object)[
                "title"     => "User",
                "getFunc"   => "getPosts_user",
                "countFunc" => "countPosts_all"
            ]
        ];
    }

    public function getTotal($sort_id){
        return call_user_func(array($this, $this->sortModes[$sort_id]->countFunc)); //$this->$func();
    }


    public function getPost($post_id, $username = NULL, $content = false){
        $result = $this->db->where("id", $post_id)
            ->limit(1)
            ->get("posts")->result();

        if (count($result) < 1){ return NULL; }

        $votes = $this->getVotes($post_id);
        $output = new stdClass();
        $output->upvotes = $votes->upvotes;
        $output->downvotes = $votes->downvotes;
        $output->votes = ($votes->upvotes - $votes->downvotes);
        $output->uservote = ($username == NULL) ? (0) : ($this->getUserVote($post_id, $username));
        $output->id = $result[0]->id;
        $output->link = $result[0]->link;
        $output->title = $result[0]->title;
        $output->date = $result[0]->posted_at;
        $output->user = $result[0]->posted_by;
        $output->content = ($content || $result[0]->link) ? ($result[0]->content) : ("");
        return $output;
    }

    public function getPosts($page, $sort_id){
        return call_user_func_array(
            array($this, $this->sortModes[$sort_id]->getFunc),
            array($page)
        );
    }

    public function getAll($page, $sort_id = SORT_DEFAULT, $username = NULL){
        $posts = $this->getPosts($page, $sort_id);
        $output = array();
        foreach ($posts->result() as $post){
            $output[] = $this->getPost($post->id, $username, false);
        }

        return $output;
    }

    public function createPost($content, $title, $user){
        $this->db->insert(
            "posts",
            array(
                "title"     => $title,
                "posted_at" => date("Y-m-d H:i:s"),
                "posted_by" => $user,
                "content"   => htmlspecialchars($content),
                "link"      => !(
                    filter_var(
                        trim($content),
                        FILTER_VALIDATE_URL,
                        FILTER_FLAG_ENCODE_LOW || FILTER_FLAG_ENCODE_HIGH || FILTER_FLAG_HOST_REQUIRED
                    ) === false
                )
            )
        );
    }


    public function getPosts_popularityAll($page){
        $prefix = DATABASE_PREFIX;
        $limit = POSTS_PER_PAGE;
        $offset = (POSTS_PER_PAGE * ($page - 1));
        return $this->db->query("
            SELECT * FROM
                ${prefix}posts,
                (SELECT post_id, SUM(vote) as votes FROM ${prefix}votes GROUP BY post_id) as v
            WHERE
                ${prefix}posts.id = v.post_id
            ORDER BY v.votes DESC
            LIMIT ${offset}, ${limit}
        ");
    }

    public function getPosts_popularityDay($page){
        $prefix = DATABASE_PREFIX;
        $limit = POSTS_PER_PAGE;
        $offset = (POSTS_PER_PAGE * ($page - 1));
        return $this->db->query("
            SELECT * FROM
                ${prefix}posts,
                (SELECT post_id, SUM(vote) as votes FROM ${prefix}votes GROUP BY post_id) as v
            WHERE
                ${prefix}posts.id = v.post_id
                AND
                UNIX_TIMESTAMP(${prefix}posts.posted_at) > (UNIX_TIMESTAMP() - 86400)
            ORDER BY v.votes DESC
            LIMIT ${offset}, ${limit}
        ");
    }

    public function getPosts_date($page){
        return $this->db->limit(POSTS_PER_PAGE, (POSTS_PER_PAGE * ($page - 1)))
            ->order_by("posted_at DESC")
            ->get("posts");
    }

    public function getPosts_title($page){
        return $this->db->limit(POSTS_PER_PAGE, (POSTS_PER_PAGE * ($page - 1)))
            ->order_by("title ASC")
            ->get("posts");

    }

    public function getPosts_user($page){
        return $this->db->limit(POSTS_PER_PAGE, (POSTS_PER_PAGE * ($page - 1)))
            ->order_by("posted_by ASC")
            ->get("posts");

    }


    public function countPosts_all(){
        return $this->db->count_all("posts");
    }

    public function countPosts_popularityDay(){
        $prefix = DATABASE_PREFIX;
        return $this->db->query("SELECT COUNT(*) AS cnt FROM ${prefix}posts WHERE UNIX_TIMESTAMP(posted_at) > (UNIX_TIMESTAMP() - 86400)")->result()[0]->cnt;
    }


    public function vote($post_id, $user, $vote){
        $this->db->replace(
            "votes",
            array(
                "post_id" => $post_id,
                "user" => $user,
                "vote" => $vote
            )
        );
    }

    public function getVotes($post_id){
        $prefix = DATABASE_PREFIX;
        $output = $this->db->query("
            SELECT * FROM
                (SELECT COUNT(vote) as upvotes FROM ${prefix}votes WHERE vote > 0 AND post_id = $post_id) as u,
                (SELECT COUNT(vote) as downvotes FROM ${prefix}votes WHERE vote < 0 AND post_id = $post_id) as d
        ")->result()[0];

        $output->upvotes = (int)$output->upvotes;
        $output->downvotes = (int)$output->downvotes;

        return $output;
    }


    public function getUserPosts($username){
        return $this->db->where("posted_by", $username)
            ->order_by("posted_at", "DESC")
            ->get("posts");
    }

    public function getUserVotes($username){
        return $this->db->where("user", $username)
            ->order_by("voted_at", "DESC")
            ->get("votes");
    }

    public function getUserVote($post_id, $username){
        $output = $this->db->where("user", $username)
            ->where("post_id", $post_id)
            ->limit(1)
            ->get("votes")->result();

        return (count($output) > 0) ? ((int)$output[0]->vote) : (0);
    }

    public function getUserPostsCount($username){
        $this->db->where("posted_by", $username);
        $this->db->from("posts");
        return $this->db->count_all_results();
    }

    public function getUserVotesCount($username){
        $this->db->where("user", $username);
        $this->db->where("vote !=", 0);
        $this->db->from("votes");
        return $this->db->count_all_results();
    }


    public function getComment($comment_id){
        return $this->db->where("id", $comment_id)
            ->limit(1)
            ->get("comments")
            ->result()[0];
    }

    public function getTopLevelComments($post_id){
        return $this->db->where("post_id", $post_id)
            ->where("parent", NULL)
            ->get("comments");
    }

    public function getCommentChildrenIDs($comment_id){
        return $this->db->select("id")
            ->where("parent", $comment_id)
            ->get("comments");
    }

    public function putComment($post_id, $user, $parent, $content){
        $this->db->insert(
            "comments",
            array(
                "post_id"   => $post_id,
                "parent"    => (($parent == NULL) || (trim($parent) == "")) ? (NULL) : ($parent),
                "user"      => $user,
                "content"   => htmlspecialchars($content),
                "posted_at" => date("Y-m-d H:i:s")
            )
        );
    }


    public function hlp_pagesFromTotal($total){
        $pages = ceil($total / POSTS_PER_PAGE);
        return ($pages < 1) ? (1) : ($pages);
    }

    public function hlp_getGravatarUrl($username, $size){
        return sprintf("https://www.gravatar.com/avatar/%s?s=%d&d=identicon&f=y", md5(sprintf("%s@westminster.ac.uk", $username)), $size);
    }


    private function _isLink($content){
        return preg_match("/^\s*([a-zA-Z]+:\/\/)?(\w+\.)+[a-zA-Z]+(\/\w*(\.\w+)?)*(\?\w+=\w*(&\w+=\w*)*)?(#[^# ])?\s*$/");
    }

    public function _generatePost(){
        $good = false;
        while (!$good){
            $s = explode(".", $this->_getLipsum(mt_rand(1, 10), mt_rand(1, 100)));
            if (count($s) < 1){ return; }
            $link = true;
            if (mt_rand(0, 1) == 0){
                if (count($s) < 3){ continue; }
                $content = implode(".", array_splice($s, 2));
                $link = false;
            } else{
                $url_parts = explode(" ", trim(strtolower($s[1])));
                if (count($url_parts) < 3){ continue; }
                $content = vsprintf("http://%s-%s.%s", array_splice($url_parts, 0, 3));
            }
            $good = true;
        }

        $this->db->insert(
            "ecwm604_cw1_posts",
            array(
                "title" => trim($s[0]),
                "link" => $link,
                "posted_at" => date("c"),
                "posted_by" => $this->_usernames[mt_rand(0, count($this->_usernames)-1)],
                "content" => htmlspecialchars($content)
            )
        );
    }

    public function _simulateVoting($voteChance){
        $total = $this->countPosts_all();
        $pages = $this->hlp_pagesFromTotal($total);
        $grandTotal = ($total * count($this->_usernames));

        $x = 0;
        foreach ($this->_usernames as $user){
            for ($page = 1; $page <= $pages; ++$page){
                $posts = $this->getPosts_date($page);
                foreach ($posts->result() as $post){
                    ++$x;

                    if ((1.0 * mt_rand() / getrandmax()) <= $voteChance){
                        $this->vote($post->id, $user, (mt_rand(0, 1) == 0) ? (1) : (-1));
                    }

                    echo ".";

                    if (($x % 10) == 0){
                        if (($x % 150) == 0){
                            echo "STILL WORKING<br>";
                        }

                        ob_flush();
                        flush();
                    }
                }
            }
        }
        echo "<br><br>";
        printf("<a href='%s'>============ DONE ============</a>", site_url(""));
    }

    public function _getLipsum($amount, $start){
        return simplexml_load_file("http://www.lipsum.com/feed/xml?amount=$amount&start=$start")->lipsum;
    }
}
