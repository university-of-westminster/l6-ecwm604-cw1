        </div>

        <?php if (!isset($this->session->username)): ?>
        <div class="page-blocker">
        <div class="username-window">
            <h4>Choose a username</h4>
            <?php echo form_open("main/login", array("name" => "username", "method" => "post", "class" => "col s12")); ?>
                <div class="row">
                    <div class="input-field col s12">
                    <input placeholder="Enter a username and click continue" id="username" name="username" type="text" class="validate hoverable" style="padding-left:10px;padding-right:10px;box-sizing:border-box;" value="<?php echo $this->session->username; ?>">
                        <input type="submit" value="Continue" class="center btn-flat green waves-effect waves-light" style="width:100%;">
                    </div>
                </div>
            </form>
        </div>
        </div>
        <?php endif; ?>

        <script src="<?php echo URL_BASE; ?>/assets/js/local-settings.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/jquery/jquery-2.1.1.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/materialize/js/materialize.min.js"></script>
        <script src="<?php echo URL_BASE; ?>/assets/js/coursework.js"></script>
        <script>
            $(function(){
                <?php
                    if ($this->session->errors){
                        foreach ($this->session->errors as $error){
                            printf("Materialize.toast('<span class=\"toast-error\">%s</span>', 4000);\n", $error);
                        }
                    }

                    if ($this->session->notifications){
                        foreach ($this->session->notifications as $notification){
                            printf("Materialize.toast('<span class=\"toast-notification\">%s</span>', 4000);\n", $notification);
                        }
                    }
                ?>
            });
        </script>
    </body>
</html>
