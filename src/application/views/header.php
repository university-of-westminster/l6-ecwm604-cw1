<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CW1<?php printf(" - %s", isset($pageTitle) ? ($pageTitle) : ("Reddit Clone")); ?></title>
        <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="<?php echo URL_BASE; ?>/assets/materialize/css/materialize.min.css" rel="stylesheet">
        <link href="<?php echo URL_BASE; ?>/assets/css/coursework.css" rel="stylesheet">
    </head>
    <body>
        <div id="container" class="container" <?php if ($this->session->username == NULL){ echo "style='filter:blur(8px);'"; } ?>>
            <nav role="navigation" class="purple darken-3">
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo right">ShareIt!</a>
                    <ul id="nav-mobile" class="left hide-on-med-and-down">
                        <li><a href="<?php echo site_url(""); ?>">Homepage</a></li>
                        <li class="purple lighten-1"><a href="<?php echo site_url("/post/submit"); ?>">Submit a post</a></li>
                        <li><a href="#!" id="container-toggle">Toggle width</a></li>
                        <li><a href="<?php echo site_url("/generate"); ?>">Generate post</a></li>
                        <li><a href="<?php echo site_url("/simulate"); ?>">Simulate votes</a></li>
                        <?php if ($this->session->username != NULL): ?>
                        <li><a href="<?php echo site_url("/logout"); ?>">Logout from '<?php echo $this->session->username; ?>' <img class="img-round" style="vertical-align:middle;" src="<?php echo $this->posts->hlp_getGravatarUrl($this->session->username, 30); ?>"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
