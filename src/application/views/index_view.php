            <div class="row" style="margin-right:0;">
                <div class="col s12 m3">
                    <h5>Sort by:</h5>
                    <ul class="collection">
                    <?php foreach ($this->posts->sortModes as $modeID => $mode): ?>
                        <a class="collection-item <?php if ($this->session->sort == $modeID){ echo "active"; } ?>" href="<?php echo site_url(sprintf("/sort/%d", $modeID)); ?>"><?php echo $mode->title; ?></a>
                    <?php endforeach; ?>
                    </ul>
                    <p>You are logged in as:</p>
                    <div class="user-info-box">
                        <h3><?php echo ($this->session->username) ? ($this->session->username) : ("username"); ?></h3>
                        <img src="<?php echo $this->posts->hlp_getGravatarUrl($this->session->username, 120); ?>">
                        <p>You have posted <strong><?php echo $userPosts; ?></strong> post<?php echo ($userPosts == 1)?(""):("s"); ?> and cast <strong><?php echo $userVotes; ?></strong> vote<?php echo ($userVotes == 1)?(""):("s"); ?>!</p>
                        <p><a href="<?php echo site_url("/logout"); ?>"><i class="material-icons" style="vertical-align:middle;">settings_power</i> Logout</a></p>
                    </div>
                </div>
                <div class="col s12 m9" style="padding:0;">
                    <div class="row" style="margin-right:0;margin-left:5px;">
                        <?php foreach ($posts as $post): ?>
                        <div class="item col s12 hoverable">
                                <div class="item-vote-section">
                                    <div class="row valign-wrapper">
                                        <span class="item-vote-total col s6 valign" style="display:block"><?php echo $post->votes; ?></span>
                                        <div class="item-vote-arrows col s6">
                                            <i class="material-icons green-text <?php if ($post->uservote != 1){ echo "text-lighten-3"; } ?> vote-up" data-id="<?php echo $post->id; ?>">call_made</i>
                                            <i class="material-icons red-text <?php if ($post->uservote != -1){ echo "text-lighten-3"; } ?> vote-down" data-id="<?php echo $post->id; ?>">call_received</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-main-section">
                                    <div class="row">
                                        <div class="item-main-title col s12">
                                            <?php
                                                printf(
                                                    "<a href='%s'>%s</a> <span>(%s)</span>",
                                                    ($post->link) ?
                                                    ($post->content) :
                                                    (
                                                        site_url(
                                                            sprintf(
                                                                "/post/%d",
                                                                $post->id
                                                            )
                                                        )
                                                    ),
                                                    $post->title,
                                                    ($post->link) ? ($post->content) : ("textpost")
                                                );
                                            ?>
                                        </div>
                                        <div class="item-main-sub col s12">
                                            <a href="<?php echo site_url(sprintf("/post/%d", $post->id)); ?>#comments">comment</a>
                                            - posted at <?php echo date("d/M/Y", strtotime($post->date)); ?> by <a href="<?php echo site_url(sprintf("/user/%s", $post->user)); ?>"><?php echo $post->user; ?></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="row">
                        <div class="col s12 center">
                            <ul class="pagination">
                                <?php if ($page < 2): ?>
                                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo site_url(sprintf("/%d", $page - 1)); ?>"><i class="material-icons">chevron_left</i></a></li>
                                <?php endif; ?>

                                <?php for ($i = 1; $i <= $this->posts->hlp_pagesFromTotal($total); ++$i): ?>
                                <li <?php if ($page == $i){ echo "class='active'"; } ?>><a href="<?php echo site_url(sprintf("/%d", $i)); ?>"><?php echo $i; ?></a></li>
                                <?php endfor; ?>

                                <?php if ($page >= $this->posts->hlp_pagesFromTotal($total)): ?>
                                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                                <?php else: ?>
                                    <li><a href="<?php echo site_url(sprintf("/%d", $page + 1)); ?>"><i class="material-icons">chevron_right</i></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
