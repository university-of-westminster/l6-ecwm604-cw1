$(function(){
    $("#container-toggle").on("click", function(){
        $("#container").toggleClass("container");
    });

    $(".vote-up").on("click", function(){
        handleVote(this, 1);
    });

    $(".vote-down").on("click", function(){
        handleVote(this, 2);
    });

    $(".comment-reply").on("click", function(){
        createReply(this);
    });
});

function vote(id, vote, callback = function(){}){
    $.get(
        URL_BASE + "/vote/" + id + "/" + vote,
        function (){ callback(); }
    );
}

function getVotes(id, callback){
    $.get(
        URL_BASE + "/votes/" + id,
        callback,
        "json"
    );
}

function handleVote(element, vote_direction){
    if ($(element).hasClass("text-lighten-3")){
        $(element).parent().children().addClass("text-lighten-3");
        $(element).removeClass("text-lighten-3");
        vote($(element).data("id"), vote_direction, function(){
            var totalVotesEl = $(element).parent().parent().find(".item-vote-total");
            getVotes($(element).data("id"), function(n){ totalVotesEl.text(n.upvotes - n.downvotes); });
        });
    } else{
        $(element).parent().children().addClass("text-lighten-3");
        vote($(element).data("id"), 0, function(){
            var totalVotesEl = $(element).parent().parent().find(".item-vote-total");
            getVotes($(element).data("id"), function(n){ totalVotesEl.text(n.upvotes - n.downvotes); });
        });
    }
}


function createReply(el){
    $(el).parent().append('<div class="col s12 reply"><form method="post" action="' + URL_BASE + '/submit/comment"><textarea name="comment">Enter your comment here</textarea><input type="hidden" name="post_id" value="' + $(el).data("post-id") + '"><input type="hidden" name="parent" value="' + $(el).data("parent-id") + '"><input type="submit" value="Submit"><button onclick="$(this).parent().remove()">Cancel</button></form></div>');
}
