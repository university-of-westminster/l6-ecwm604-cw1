DROP TABLE IF EXISTS `ecwm604_cw1_comments`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw1_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `user` varchar(64) COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `post_id` (`post_id`,`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw1_posts`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw1_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) COLLATE utf8_bin NOT NULL,
  `posted_at` datetime NOT NULL,
  `link` tinyint(1) NOT NULL,
  `posted_by` varchar(64) COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `posted_at` (`posted_at`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `ecwm604_cw1_votes`;
CREATE TABLE IF NOT EXISTS `ecwm604_cw1_votes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user` varchar(64) COLLATE utf8_bin NOT NULL,
  `vote` smallint(6) NOT NULL,
  `voted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`,`user`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;
